Installation:

- Extract modul files to your moduls directory (sites/*/modules)
- Enable module on admin/build/modules


Configuration:

- Goto admin/store/settings/payment/edit/methods
- Open fieldset "Masterpayment settings"
- Insert all necessary values (mail, secret)
- Enable/Disable specific masterpayment methods
