<?php

/**
 * @file
 * Masterpayment administration menu items.
 */

/**
 * Process Instant Payment Notifiations from Masterpayment.
 */
function uc_masterpayment_ipn() {
  if (!isset($_POST['TX_ID'])) {
    watchdog('uc_masterpayment', 'IPN attempted with invalid transaction ID.', array(), WATCHDOG_ERROR);
    return;
  }

  $order_id = intval($_POST['TX_ID']);

  watchdog('uc_masterpayment', 'Receiving IPN at URL for order @order_id. <pre>@debug</pre>', array('@order_id' => $order_id, '@debug' => variable_get('uc_masterpayment_debug_ipn', FALSE) ? print_r($_POST, TRUE) : ''));

  $order = uc_order_load($order_id);

  if ($order == FALSE) {
    watchdog('uc_masterpayment', 'IPN attempted for non-existent order @order_id.', array('@order_id' => $order_id), WATCHDOG_ERROR);
    return;
  }

  // Assign posted variables to local variables
  $data['order_id'] = $order_id;
  $data['payment_method'] = check_plain($_POST['METHOD']);
  $data['payment_status_text'] = check_plain($_POST['STATUS_TEXT']);
  $data['payment_cust_id'] = check_plain($_POST['CUST_ID']);
  $data['payment_status'] = check_plain($_POST['STATUS']);
  $data['payment_currency'] = check_plain($_POST['CURRENCY']);
  $data['payment_value'] = check_plain($_POST['VALUE']);
  $data['payment_fee'] = check_plain($_POST['FEE']);
  $data['payment_disagio'] = check_plain($_POST['DISAGIO']);
  $data['payment_final_value'] = check_plain($_POST['FINAL_VALUE']);
  $data['payment_invoice_no'] = check_plain($_POST['INVOICE_NO']);
  $data['payment_customer_no'] = check_plain($_POST['CUSTOMER_NO']);

  $data = array_filter($data);

  $payment_ctrl_key = check_plain($_POST['CTRL_KEY']);

  // Verify transaction notification
  $local_ctrl_key = md5(implode('|', $data) . '|' . variable_get('uc_masterpayment_gateway_secret', ''));
  if ($local_ctrl_key == $payment_ctrl_key) {
    watchdog('uc_masterpayment', 'IPN transaction verified. ' . print_r($data, TRUE));

    $context = array(
      'revision' => 'formatted-original',
      'type' => 'amount',
    );
    $options = array(
      'sign' => FALSE,
    );

    switch ($data['payment_status']) {
      case 'PENDING':
        uc_order_update_status($order_id, 'masterpayment_pending');
        uc_order_comment_save($order_id, 0, t('Masterpayment gateway invocation succeeded, payment is waiting for manual activation by merchant'), 'admin');
        break;

      case 'SCHEDULED':
        uc_order_update_status($order_id, 'masterpayment_scheduled');
        uc_order_comment_save($order_id, 0, t('Masterpayment gateway invocation succeeded, payment is scheduled for automatic execution'), 'admin');
        break;

      case 'SUCCESS':
        $payment_amount = $data['payment_value'] / 100;
        $comment = t('Masterpayment transaction');
        uc_payment_enter($order_id, $order->payment_method, $payment_amount, $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through Masterpayment.', array('@amount' => uc_price($payment_amount, $context, $options), '@currency' => $payment_currency)), 'order', 'payment_received');
        uc_order_comment_save($order_id, 0, t('Masterpayment IPN reported a payment of @amount @currency.', array('@amount' => uc_price($payment_amount, $context, $options), '@currency' => $data['payment_currency'])));
        break;

      case 'CANCELLED':
        uc_order_comment_save($order_id, 0, t('Masterpayment payment canceled manually by customer.'), 'admin');
        break;

      case 'REVOKED':
        uc_order_comment_save($order_id, 0, t('Masterpayment payment revoked manually by merchant.'), 'admin');
        break;

      case 'TIMED_OUT':
        uc_order_comment_save($order_id, 0, t('Masterpayment payment has timed out waiting for input from customer.'), 'admin');
        break;

      case 'REFUSED_RISK':
        uc_order_comment_save($order_id, 0, t('Masterpayment payment was refused due to risk assessment.'), 'admin');
        break;

      case 'FAILED':
        uc_order_comment_save($order_id, 0, t('Masterpayment payment failed.'), 'admin');
        break;
    }
  }
  else {
    watchdog('uc_masterpayment', 'IPN transaction failed verification.', array(), WATCHDOG_ERROR);
    uc_order_comment_save($order_id, 0, t('An IPN transaction failed verification for this order.'), 'admin');
  }
}

function uc_masterpayment_gateway() {
  return '<iframe width="100%" frameborder="0" height="400" src="' . url(variable_get('uc_masterpayment_gateway_url', 'https://www.masterpayment.com/en/payment/gateway'), array('query' => $_POST, 'absolute' => TRUE)) . '"></iframe>';
}

// Handles a successfull payment.
function uc_masterpayment_success($order_id) {
  // If the order ID specified in the return URL is not the same as the one in
  // the user's session, we need to assume this is either a spoof or that the
  // user tried to adjust the order on this side while at PayPal. If it was a
  // legitimate checkout, the IPN will still come in from PayPal so the order
  // gets processed correctly. We'll leave an ambiguous message just in case.
  if (intval($_SESSION['cart_order']) != $order_id) {
    drupal_set_message(t('Thank you for your order! Masterpayment will notify us once your payment has been processed.'));
    drupal_goto('cart');
  }

  $order = uc_order_load($order_id);

  // Ensure the payment method is one of Masterpayment
  if (drupal_substr($order->payment_method, 0, 14) != 'masterpayment_') {
    drupal_goto('cart');
  }

  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['do_complete'] = TRUE;

  drupal_goto('cart/checkout/complete');
}

// Handles a canceled payment.
function uc_masterpayment_cancel() {
  $_SESSION['uc_masterpayment_try'] += 1;
  drupal_set_message(t('You have cancelled the payment, now feel free to continue shopping using another payment method.'));
  drupal_goto(variable_get('uc_masterpayment_cancel_return_url', 'cart'));
}

// Handles a failed payment.
function uc_masterpayment_failure() {
  $_SESSION['uc_masterpayment_try'] += 1;
  drupal_set_message(t('Your payment has failed. Please feel free to continue shopping using another payment method or contact us for assistance.'));
  drupal_goto(variable_get('uc_masterpayment_failure_return_url', 'cart'));
}
